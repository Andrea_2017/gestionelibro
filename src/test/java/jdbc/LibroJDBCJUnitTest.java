package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import vo.Libro;

public class LibroJDBCJUnitTest {

	@Test
	public  void getAllLibri() {
		List <Libro> libri = new ArrayList<>();
		Libro l = new Libro();
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";

		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "select * from Libro";

			ResultSet rs = stmt.executeQuery(sql);
			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				l.setLibroId(rs.getInt("LIBROID"));
				l.setAutore(rs.getString("AUTORE"));
				l.setDataOraUltimoAggiornamento(rs.getDate("DATAORAULTIMOAGGIORNAMENTO"));
				l.setNumeroPagine(rs.getInt("NUMEROPAGINE"));
				l.setTitolo(rs.getString("TITOLO"));
				libri.add(l);
				l = new Libro();
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

		System.out.println("Goodbye!");

	}
	
	@Test
	public void getLibroById() {
		int idLibro = 15;
		Libro libro = new Libro();
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";

		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = " SELECT * FROM libro where LIBROID =  '" + idLibro + "' ORDER by titolo ";

			ResultSet rs = stmt.executeQuery(sql);
			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				libro.setLibroId(rs.getInt("LIBROID"));
				libro.setAutore(rs.getString("AUTORE"));
				libro.setDataOraUltimoAggiornamento(rs.getDate("DATAORAULTIMOAGGIORNAMENTO"));
				libro.setNumeroPagine(rs.getInt("NUMEROPAGINE"));
				libro.setTitolo(rs.getString("TITOLO"));
				break;
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

		System.out.println("Goodbye!");
	}
	
	public static void main(String[] args) {
		new LibroJDBCJUnitTest().getAllLibri();
		//updateLibro(21);
	}
	
	public  void insert() {
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";

		Connection conn = null;
		String sql = "INSERT INTO TEST_JAVA_DEV.LIBRO "
				+ "(LIBROID, AUTORE, DATAORAULTIMOAGGIORNAMENTO, NUMEROPAGINE, TITOLO) "
				+ "VALUES"
				+ "( ?, ?, ?, ?, ? )";
		PreparedStatement pstmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			pstmt = conn.prepareStatement(sql);
			
			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			pstmt.setInt(1, 29);
			pstmt.setString(2, "Test Autore jdbc 2");
			pstmt.setTimestamp(3, new Timestamp(new Date().getTime()));
			
			pstmt.setInt(4, 450);
			pstmt.setString(5, "Test Titolo JDBC 2");
			
			int rowsUpdated = pstmt.executeUpdate();
			if (rowsUpdated > 0) {
			    System.out.println("An existing Libro was updated successfully!");
			}
			
			pstmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		System.out.println("Inser Completato!");
	}
	
	public static void updateLibro(int idLibro) {
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";
		
		Connection conn = null;
		String sql = " update Libro"
				+ " set Libro.autore = ?, Libro.titolo = ?, Libro.DATAORAULTIMOAGGIORNAMENTO = CURRENT_TIMESTAMP, Libro.NUMEROPAGINE = ?"
				+ " where Libro.LIBROID = ?";
		/*
		 * 1:	autore
		 * 2:	titolo
		 * 3:	numeroPagine
		 * 4:	libroId
		 */
		PreparedStatement pstmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			pstmt = conn.prepareStatement(sql);
			
			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			pstmt.setString(1, "Test JDBC UPDATE");
			pstmt.setString(2, "Test JDBC UPDATE");
			pstmt.setInt(3, 450);
			pstmt.setInt(4, idLibro);
			
			int rowsUpdated = pstmt.executeUpdate();
			if (rowsUpdated > 0) {
			    System.out.println("An existing Libro was updated successfully!");
			}
			
			pstmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		System.out.println("Inser Completato!");
	}
}
