package dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import vo.Libro;

public class LibroDAO {
	
	public static void main(String[] args) {
		/* 
		 * recupero prossima chiave disponibile */
		/*
		int prossimaChiave = new LibroDAO().getProssimaChiave();
		System.out.println(prossimaChiave);
		*/
		/*
		 * recupero lista libri */
		/*
		//System.out.println(new LibroDAO().getAllLibri());
		//Libro l = new LibroDAO().getLibroById(15);
		/*
		/* 
		 * test inserimento libro */
		/*
		Libro libro = new Libro( "TEST TITOLO 2 2 18", "TEST AUTORE 2 2 18", 350);
		int libriModificati = new LibroDAO().updateLibro(19, libro);
		System.out.println(libriModificati);
		//System.out.println(l);
		 */
		/*
		int libroInserito = new LibroDAO().insert(new Libro("TEST TIT 3 2 18 16 52", "TEST AUT 3 2 18 16 52", 500));
		System.out.println(libroInserito);
		*/
		
		//new LibroDAO().eliminaLibro(24);
		
		/* test rest services */
		/*
		List<Libro> libri = new LibroDAO().getLibriByAuthor("testtt autore");
		System.out.println(libri);
		*/
		/*
		List<Libro> libri = new LibroDAO().getLibriWithAtLeastPages(900);
		System.out.println(libri);
		*/
	}
	
	
	public int getProssimaChiave() {
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";

		Connection conn = null;
		Statement stmt = null;
		int nextKey = -1;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = " SELECT LIBRO_SEQUENCE.NEXTVAL FROM DUAL";

			ResultSet rs = stmt.executeQuery(sql);
			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				nextKey = rs.getInt("NEXTVAL");
				break;
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

		System.out.println("la prossima chiave disponibile è la: " + nextKey);
		return nextKey;
	}
	
	public List<Libro> getAllLibri() {
		List<Libro> libri = new ArrayList<>();
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";

		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT * FROM libro ORDER by titolo";

			ResultSet rs = stmt.executeQuery(sql);
			// STEP 5: Extract data from result set
			Libro libro = new Libro();
			while (rs.next()) {
				// Retrieve by column name
				libro.setLibroId(rs.getInt("LIBROID"));
				libro.setAutore(rs.getString("AUTORE"));
				libro.setDataOraUltimoAggiornamento(rs.getDate("DATAORAULTIMOAGGIORNAMENTO"));
				libro.setNumeroPagine(rs.getInt("NUMEROPAGINE"));
				libro.setTitolo(rs.getString("TITOLO"));
				libri.add(libro);
				libro = new Libro();
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

		System.out.println("Goodbye!");
		return libri;
	}

	public Libro getLibroById(int idLibro) {
		Libro libro = new Libro();
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";

		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = " SELECT * FROM libro where LIBROID =  '" + idLibro + "' ORDER by titolo ";

			ResultSet rs = stmt.executeQuery(sql);
			// STEP 5: Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				libro.setLibroId(rs.getInt("LIBROID"));
				libro.setAutore(rs.getString("AUTORE"));
				libro.setDataOraUltimoAggiornamento(rs.getDate("DATAORAULTIMOAGGIORNAMENTO"));
				libro.setNumeroPagine(rs.getInt("NUMEROPAGINE"));
				libro.setTitolo(rs.getString("TITOLO"));
				break;
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

		System.out.println("Goodbye!");
		return libro;
	}
	

	public int insert(Libro libroAttrNuovi) {
		int nuovoIdLibro = getProssimaChiave();
		int libroInserito = -1;
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";

		Connection conn = null;
		String sql = "INSERT INTO TEST_JAVA_DEV.LIBRO "
				+ "(LIBROID, AUTORE, DATAORAULTIMOAGGIORNAMENTO, NUMEROPAGINE, TITOLO) " + "VALUES"
				+ "( ?, ?, CURRENT_TIMESTAMP, ?, ? )";
		/*
		 * 1.	ID
		 * 2.	AUTORE
		 * 3.	NUMEROPAGINE
		 * 4.	TITOLO
		 */
		PreparedStatement pstmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			pstmt = conn.prepareStatement(sql);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			pstmt.setInt(1, nuovoIdLibro);
			pstmt.setString(2, libroAttrNuovi.getAutore());

			pstmt.setInt(3, libroAttrNuovi.getNumeroPagine());
			pstmt.setString(4, libroAttrNuovi.getTitolo());

			libroInserito = pstmt.executeUpdate();
			if (libroInserito > 0) {
				System.out.println("the book " + libroAttrNuovi + "was inserted successfully!");
			}
			pstmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		System.out.println("Insert Completed!");
		return libroInserito;
	}
	
	public int updateLibro(int idLibro, Libro libroAttrModificati) {
		int rowsUpdated = -1;
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";
		
		Connection conn = null;
		String sql = " update Libro"
				+ " set Libro.autore = ?, Libro.titolo = ?, Libro.DATAORAULTIMOAGGIORNAMENTO = CURRENT_TIMESTAMP, Libro.NUMEROPAGINE = ?"
				+ " where Libro.LIBROID = ?";
		/*
		 * 1:	autore
		 * 2:	titolo
		 * 3:	numeroPagine
		 * 4:	libroId
		 */
		PreparedStatement pstmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			pstmt = conn.prepareStatement(sql);
			
			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			pstmt.setString(1, libroAttrModificati.getAutore());
			pstmt.setString(2, libroAttrModificati.getTitolo());
			pstmt.setInt(3, libroAttrModificati.getNumeroPagine());
			pstmt.setInt(4, idLibro);
			
			rowsUpdated = pstmt.executeUpdate();
			if (rowsUpdated > 0) {
			    System.out.println("An existing Libro was updated successfully!");
			}
			
			pstmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		System.out.println("Inser Completato!");
		return rowsUpdated;
	}
	
	public int eliminaLibro(int idLibro) {
		int rowsUpdated = -1;
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";
		
		Connection conn = null;
		String sql = " delete LIBRO where LIBROID = ?";
		/*
		 * 1:	autore
		 * 2:	titolo
		 * 3:	numeroPagine
		 * 4:	libroId
		 */
		PreparedStatement pstmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			pstmt = conn.prepareStatement(sql);
			
			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			pstmt.setInt(1, idLibro);
			
			rowsUpdated = pstmt.executeUpdate();
			if (rowsUpdated > 0) {
			    System.out.println("An existing Libro was deleted successfully!");
			}
			
			pstmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		System.out.println("Deleting Completed!");
		return rowsUpdated;
	}
	
	/* chiamate dai rest services */
	
	public List<Libro> getLibriByAuthor(String author) {
		List<Libro> libri = new ArrayList<>();
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";

		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "select * from Libro where AUTORE like '%" + author + "%'";

			ResultSet rs = stmt.executeQuery(sql);
			// STEP 5: Extract data from result set
			Libro libro = new Libro();
			while (rs.next()) {
				// Retrieve by column name
				libro.setLibroId(rs.getInt("LIBROID"));
				libro.setAutore(rs.getString("AUTORE"));
				libro.setDataOraUltimoAggiornamento(rs.getDate("DATAORAULTIMOAGGIORNAMENTO"));
				libro.setNumeroPagine(rs.getInt("NUMEROPAGINE"));
				libro.setTitolo(rs.getString("TITOLO"));
				libri.add(libro);
				libro = new Libro();
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

		System.out.println("Goodbye!");
		return libri;
	}
	
	/* elenco di tutti i libri con più N pagine */
	public List<Libro> getLibriWithAtLeastPages(int numeroMinimoPagine) {
		List<Libro> libri = new ArrayList<>();
		final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
		final String DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";

		// Database credentials
		final String USER = "TEST_JAVA_DEV";
		final String PASS = "Pa9ss_word";

		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "select * from Libro where NUMEROPAGINE >= " + numeroMinimoPagine;

			ResultSet rs = stmt.executeQuery(sql);
			// STEP 5: Extract data from result set
			Libro libro = new Libro();
			while (rs.next()) {
				// Retrieve by column name
				libro.setLibroId(rs.getInt("LIBROID"));
				libro.setAutore(rs.getString("AUTORE"));
				libro.setDataOraUltimoAggiornamento(rs.getDate("DATAORAULTIMOAGGIORNAMENTO"));
				libro.setNumeroPagine(rs.getInt("NUMEROPAGINE"));
				libro.setTitolo(rs.getString("TITOLO"));
				libri.add(libro);
				libro = new Libro();
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

		System.out.println("Goodbye!");
		return libri;
	}

}
