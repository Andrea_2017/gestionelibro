package com.hellokoding.springboot;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dao.LibroDAO;
import vo.Libro;

@Controller
public class CreateUpdateDeleteLibroController {
	@RequestMapping("/creUpdDelLibro")
	public String hello(Model model, HttpServletRequest request, HttpServletResponse response,
		@RequestParam(value = "titolo", required = false) String titolo,
		@RequestParam(value = "autore", required = false) String autore,
		@RequestParam(value = "numeroPagine", required = false) String numeroPagine,
		@RequestParam(value = "action", required = true) String action) {
		if(action.startsWith("update")){
			int idModifica =  Integer.parseInt(action.substring(7));
			Libro libroDaModificare = new LibroDAO().getLibroById(idModifica);
			System.out.println(idModifica);
			model.addAttribute("libroDaModificare", libroDaModificare);
			return "editLibro";
		}else if(action.startsWith("effettuaModificaaDB")){
			int idModifica =  Integer.parseInt(action.substring(20));
			Libro libroDaModificare = new Libro(titolo, autore, Integer.parseInt(numeroPagine));
			int libroModificato = new LibroDAO().updateLibro(idModifica, libroDaModificare);
			model.addAttribute("libriList", new LibroDAO().getAllLibri());
			System.out.println("Libri Modificati: " + libroModificato);
		}
		else if(action.startsWith("insert")){
			return "inserisciLibro";
		}
		else if(action.startsWith("effettuaInsert")){
			Libro libroDaInserire = new Libro(titolo, autore, Integer.parseInt(numeroPagine));
			new LibroDAO().insert(libroDaInserire);
			List<Libro> libriDB = new LibroDAO().getAllLibri();
			model.addAttribute("libriList", libriDB);
			return "libroHome";
		}else if(action.startsWith("elimina")){
			int idElimina =  Integer.parseInt(action.substring(8));
			new LibroDAO().eliminaLibro(idElimina);
			model.addAttribute("libriList", new LibroDAO().getAllLibri());
			return "libroHome";
		}else if(action.startsWith("goToHome")){
			model.addAttribute("libriList", new LibroDAO().getAllLibri());
			return "libroHome";
		}
		return "libroHome";
	}
}