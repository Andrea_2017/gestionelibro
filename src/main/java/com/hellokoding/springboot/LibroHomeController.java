package com.hellokoding.springboot;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dao.LibroDAO;
import vo.Libro;

@Controller
public class LibroHomeController {
	@RequestMapping("/libroApp")
	public String hello(Model model, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "name", required = false, defaultValue = "World") String name) {
		List<Libro> libriDB = new LibroDAO().getAllLibri();
		model.addAttribute("libriList", libriDB);
		return "libroHome";
	}
	
	@RequestMapping("/ricercaByAuthor")
	public String ricercaByAuthor(Model model, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "author", required = false) String author) {
		List<Libro> libriDB = new LibroDAO().getLibriByAuthor(author);
		model.addAttribute("libriList", libriDB);
		model.addAttribute("author", author);
		return "libroHome";
	}
	
	@RequestMapping("/searchWithAtLeastNPages")
	public String searchWithAtLeastNPages(Model model, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "numeroMinimoPagine", required = false) Integer numeroMinimoPagine) {
		List<Libro> libriDB = new LibroDAO().getLibriWithAtLeastPages(numeroMinimoPagine);
		model.addAttribute("libriList", libriDB);
		model.addAttribute("numeroMinimoPagine", numeroMinimoPagine);
		return "libroHome";
	}
}