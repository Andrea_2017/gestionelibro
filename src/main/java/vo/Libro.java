package vo;

import java.io.Serializable;
import java.util.Date;


public class Libro implements Serializable {

	private static final long serialVersionUID = 6672245084865336600L;
	private int libroId;
	private String titolo;
	private String autore;
	private int numeroPagine = -1;
	private Date dataOraUltimoAggiornamento;

	public int getLibroId() {
		return libroId;
	}

	public Libro() {
		super();
	}

	/**
	 * @param libroId
	 * @param titolo
	 * @param autore
	 * @param numeroPagine
	 * @param dataOraUltimoAggiornamento
	 */
	public Libro(int libroId, String titolo, String autore, int numeroPagine, Date dataOraUltimoAggiornamento) {
		super();
		this.libroId = libroId;
		this.titolo = titolo;
		this.autore = autore;
		this.numeroPagine = numeroPagine;
		this.dataOraUltimoAggiornamento = dataOraUltimoAggiornamento;
	}
	
	/**
	 * @param titolo
	 * @param autore
	 * @param numeroPagine
	 * @param dataOraUltimoAggiornamento
	 */
	public Libro( String titolo, String autore, int numeroPagine, Date dataOraUltimoAggiornamento) {
		super();
		this.titolo = titolo;
		this.autore = autore;
		this.numeroPagine = numeroPagine;
		this.dataOraUltimoAggiornamento = dataOraUltimoAggiornamento;
	}
	
	public Libro( String titolo, String autore, int numeroPagine) {
		super();
		this.titolo = titolo;
		this.autore = autore;
		this.numeroPagine = numeroPagine;
	}

	public void setLibroId(int libroId) {
		this.libroId = libroId;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getAutore() {
		return autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	public int getNumeroPagine() {
		return numeroPagine;
	}

	public void setNumeroPagine(int numeroPagine) {
		this.numeroPagine = numeroPagine;
	}

	public Date getDataOraUltimoAggiornamento() {
		return dataOraUltimoAggiornamento;
	}

	public void setDataOraUltimoAggiornamento(Date dataOraUltimoAggiornamento) {
		this.dataOraUltimoAggiornamento = dataOraUltimoAggiornamento;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("\n----LIBRO ----\n");
		sb.append("id=" + libroId + "\n");
		sb.append("titolo=" + titolo + "\n");
		sb.append("autore=" + autore + "\n");
		sb.append("numeroPagine=" + numeroPagine + "\n");
		sb.append("dataOraUltimoAggiornamento=" + dataOraUltimoAggiornamento + "\n");
		sb.append("----LIBRO ----\n");
		return sb.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (libroId ^ (libroId >>> 32));
		result = prime * result + ((titolo == null) ? 0 : titolo.hashCode());
		result = prime * result + ((autore == null) ? 0 : autore.hashCode());
		result = prime * result + (int) (numeroPagine ^ (numeroPagine >>> 32));
		result = prime * result + ((dataOraUltimoAggiornamento == null) ? 0 : dataOraUltimoAggiornamento.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Libro))
			return false;
		Libro other = (Libro) obj;

		if (libroId == other.libroId)
			return true;
		else
			return false;
	}
}