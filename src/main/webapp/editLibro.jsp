<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Libro Application</title>
<link rel="stylesheet" href="css/screen.css">
<link rel="stylesheet" href="css/style.css">
<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/jquery.validate.js"></script>
</head>
<body>
	<%@page import="vo.Libro,dao.LibroDAO"%>


	<form action="creUpdDelLibro">
		<fieldset  style="background-color: lightgreen;">
			<label><h3>Modifica Libro</h3></label>
			<table>
				<tr>
					<th>Titolo</th>
					<th>Autore</th>
					<th>Numero Pagine</th>
				</tr>
				<tr>
					<td><input type="text" value="${libroDaModificare.titolo}" name="titolo" required /></td>
					<td><input type="text" value="${libroDaModificare.autore}" name="autore" required /></td>
					<td><input type="number" value="${libroDaModificare.numeroPagine}" name="numeroPagine" min="0" required /></td>
				</tr>
				<tr>
					<td colspan="2">
						<button type="submit" name="action"
							value="effettuaModificaaDB_${libroDaModificare.libroId}"
							style="margin-top: 10%;">Effettua la Modifica</button>
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
	<form action="libroApp">
		<button type="submit" name="action" value="goToHome" style="margin-top: 10%;">Torna alla lista</button>
	</form>
</body>
</html>