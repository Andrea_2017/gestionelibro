<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="dao.LibroDAO,vo.Libro"%>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Libro Application</title>
</head>
<body>

	<fieldset style="background-color: lightblue;">
		<label>Elenco Libri</label>
		<form action="creUpdDelLibro">
			<table>
				<tr>
					<th>AUTORE</th>
					<th>TITOLO</th>
					<th>NUMEROPAGINE</th>
					<th>DATAORAULTIMOAGGIORNAMENTO</th>
					<th>MODIFICA</th>
					<th>ELIMINA</th>
				</tr>
				<c:forEach items="${libriList}" var="item">
					<tr>
						<td style="text-align: center; width: 25%;"><c:out
								value="${item.autore}" /></td>
						<td style="text-align: center; width: 25%;"><c:out
								value="${item.titolo}" /></td>
						<td style="text-align: center; width: 25%;"><c:out
								value="${item.numeroPagine}" /></td>
						<td style="text-align: center; width: 25%;"><c:out
								value="${item.dataOraUltimoAggiornamento}" /></td>
						<td style="text-align: center; width: 25%;">
							<button type="submit" name="action"
								value="update_${item.libroId}">Update</button>
						</td>
						<td style="text-align: center; width: 25%;">
							<button type="submit" name="action"
								value="elimina_${item.libroId}">Elimina</button>
						</td>
					</tr>
				</c:forEach>
				<tr style="height: 20%">
				</tr>
				<tr style="height: 20%">
					<td colspan="6" style="text-align: center;">
						<button type="submit" name="action" value="insert"
							style="margin-top: 3%;">Inserisci Libro</button>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<fieldset style="margin-top: 2%;background-color: lightgray;" >
		<label>Ricerca nell elenco i libri dell autore</label>
		<form action="ricercaByAuthor">
			<table>
				<tr>
					<td style="text-align: left; width: 100%;"><input type="text"
						value="" name="author" /></td>
				</tr>
				<tr>
					<td style="text-align: left; width: 100%;">
						<button type="submit" name="action" value="ricercaByAuthor">RICERCA</button>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>
	<fieldset style="margin-top: 2%;background-color: lightgreen;">
		<label>Ricerca dei Libri che hanno almeno N pagine</label>
		<form action="searchWithAtLeastNPages">
			<table>
				<tr>
					<td style="text-align: left; width: 100%;"><input type="text"
						value="" name="numeroMinimoPagine" value="${numeroMinimoPagine}" />
					</td>
				</tr>
				<tr>
					<td style="text-align: left; width: 100%;">
						<button type="submit" name="action" value="ricercaByAuthor">Effettua
							la Ricerca</button>
					</td>
				</tr>
			</table>
		</form>
	</fieldset>

	<form action="libroApp">
		<table>
			<tr>
				<td><input type="submit" value="Ricarica Elenco Libri"
					value="Ricarica tutto l elenco dei libri"
					style="margin-left: 610px; margin-top: 20px;" /></td>
			</tr>
		</table>
	</form>
</body>
</html>