<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Libro Application</title>
<link rel="stylesheet" href="css/screen.css">
<link rel="stylesheet" href="css/style.css">
<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/jquery.validate.js"></script>
</head>
<body>
	<fieldset style="background-color: lightgreen;">
		<label><h3>Inserimento Libro</h3></label>
		<form action="creUpdDelLibro">
			<table>
				<tr>
					<th style="text-align: center; width: 40%;">TITOLO</th>
					<th style="text-align: center; width: 40%;">AUTORE</th>
					<th style="text-align: center; width: 20%;">NUMEROPAGINE</th>
				</tr>
				<tr>
					<td style="text-align: center; width: 40%;"><input type="text"
						value="" name="titolo" required /></td>
					<td style="text-align: center; width: 40%;"><input type="text"
						value="" name="autore" required /></td>
					<td style="text-align: center; width: 20%;"><input
						type="number" value="" name="numeroPagine" min="0" required /></td>
				</tr>
			</table>
			<button type="submit" name="action"
				value="effettuaInsertaDB_${libroDaModificare.libroId}"
				style="margin-top: 10%;">Procedi con l Inserimento</button>
		</form>
	</fieldset>
	<form action="libroApp">
		<button type="submit" name="action" value="goToHome"
			style="margin-top: 10%;">Torna alla lista</button>
	</form>
</body>
</html>